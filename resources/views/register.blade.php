<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Sign Up</title>
  </head>
  <body>
    <h1>Buat Akun Baru</h1>
    <form action="/welcome" method="POST">
      @csrf
      <h2>Sign Up Form</h2>
      <label for="firstname">First Name:</label><br />
      <input type="text" name="firstname" /><br /><br />
      <label for="lastname">Last Name:</label><br />
      <input type="text" name="lastname" /><br /><br />
      <label for="gender">Gender</label><br /><br />
      <input type="radio" value="Male" name="gender" />Male<br />
      <input type="radio" value="Female" name="gender" />Female<br /><br />
      <label for="nationality">Nationality</label><br /><br />
      <select name="nationality" id="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapore">Singapore</option>
      </select><br /><br />
      <label for="language">Language Spoken</label><br /><br />
      <input type="checkbox" name="language" value="Bahasa Indonesia" />Bahasa Indonesia<br />
      <input type="checkbox" name="language" value="English" />English<br />
      <input type="checkbox" name="language" value="Other" />Other<br /><br />
      <label for="bio">Bio</label><br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
