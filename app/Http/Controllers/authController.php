<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $lastname = $request->input('lastname');

        return view('welcome', ['name' => $lastname]);
        // dd();
    }
}
